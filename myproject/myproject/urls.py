from django.contrib import admin
from django.urls import include, path
from .views import *

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("project.urls")),
    path("result_data", result, name="result_data"),
    path("logout/", logout_attempt, name="logout")
]
