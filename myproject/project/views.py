from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.contrib.auth.models import User
from .models import *
import uuid
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from .helper import send_forget_password_mail

# Create your views here.
def dashboard(request):
    return render(request, "autentikasi/index.html")

@login_required(login_url = "/")
def home(request):
    
    if request.method == "GET":
        return render(request, "home.html")

def change_password(request, token):
    context = {}
    
    try:
        profile_obj = Profile.objects.filter(forget_password_token = token).first()
        context = {'user_id' : profile_obj.user.id}
        
        if request.method == 'POST':
            new_password = request.POST.get('new_password')
            confirm_password = request.POST.get('confirm_password')
            user_id = request.POST.get('user_id')
            
            if user_id is None:
                message.success(request, 'User Tidak ditemukan.')
                return redirect(f'/change_password/{token}/')
                
            if new_password != confirm_password:
                message.success(request, 'both should be equal.')
                return redirect(f'/change_password/{token}/')
            
            user_obj = User.object.get(id = user_id)
            user_obj.set_password(new_password)
            user_obj.save()
            return redirect('/login/')
        
        context = {'user_id' : profile_obj.user.id}
    except Exception as e:
        print(e)
    return render(request, "change_password.html", context)

import uuid
def forget_password(request):
    try:
        if request.method == "POST":
        
            email = request.POST.get('email')
            
            if email == "":
                messages.success(request,  "Email Tidak Boleh Kosong")
                return redirect("forget_password")
            
            if not User.objects.filter(email=email).first():
                messages.success(request, 'Username Tidak Ditemukan')
                return redirect('/forget_password')
            
            user_obj = User.objects.filter(email = email).first()
            # user.objects.get(username = username)
            token = str(uuid.uuid4())
            
            profile_obj = get_object_or_404(Profile, user = user_obj)
            profile_obj.auth_token = token
            profile_obj.save()
            
            send_password(email, token, user_obj)
            
            messages.success(request, 'Cek Email Anda.')
            return redirect('/forget_password')
        
    except Exception as e:
        print(e)
    return render(request, "autentikasi/forget_password.html")
    
def register(request):

    if request.method == "GET":
        
        user = User.objects.filter(username = request.user).first();
        
        if user == None:
        # if user is None:
            return render(request, "autentikasi/register.html")
        else:
            return redirect("/home")

    if request.method == "POST":
        username = request.POST.get("username")
        fname = request.POST.get("fname")
        lname = request.POST.get("lname")
        email = request.POST.get("email")
        pass1 = request.POST.get("pass1")
        pass2 = request.POST.get("pass2")
        
        try:
            
            if pass1 != pass2:
                messages.success(request, "Konfirmasi Password Tidak Sesuai")
                return redirect("/register")
                
            if User.objects.filter(username = username).first():
                messages.success(request, "Username Sudah Terdaftar")
                return redirect("/register")
            
            if User.objects.filter(email = email).first():
                messages.success(request, "Email Sudah Terdaftar, Silahkan Cek Email Anda!")
                return redirect("/register")
                
            user_obj = User(username = username, email = email, first_name = fname, last_name = lname)
            user_obj.set_password(pass1)
            user_obj.save()
            
            auth_token = str(uuid.uuid4())
            profile_obj = Profile.objects.create(user = user_obj, auth_token = auth_token)
            profile_obj.save()

            send_mail_registration(email, auth_token, fname)
        
            messages.success(request, "Konfirmasi Cek Email Anda!")
            return redirect("/register")
        
        except Exception as e:
            print(e)
    
def login_attempt(request):
    
    if request.method == "GET":
        
        user = User.objects.filter(username = request.user).first();
        
        if user == None:
            return render(request, "autentikasi/login.html")
        else:
            if request.user == "":
                return redirect("/login")
            else:
                return redirect("/home")
            
        
    if request.method == "POST":
        username = request.POST.get('username')
        pass1 = request.POST.get('pass1')
        
        user_objek = User.objects.filter(username = username).first()
        
        if user_objek is None:
            messages.success(request, "Akun Tidak Ditemukan")
            return redirect("/login")
            
        user = User
        profile = Profile.objects.filter(user = user_objek).first()
        
        if not profile.is_verified:
            messages.success(request, "Profile Tidak Terverifikasi, Silahkan Cek Email")
            return redirect("/login")
            
        user = authenticate(username = username, password = pass1)
        
        if user is None:
            messages.success(request, "Password Salah")
            return redirect("/login")
            
        login(request, user)
        return redirect("/home")
    
    # except Exception as e:
    #     print(e)
    return render(request, "/login")

def verify(request, auth_token):
    try:
        
        profile_obj = Profile.objects.filter(auth_token = auth_token).first();
        if profile_obj:
            if profile_obj.is_verified:
                messages.success(request, "Akun Anda Sudah di Verifikasi")
                return redirect("/login")
                
            profile_obj.is_verified = True
            profile_obj.save()
            
            messages.success(request, "Akun Anda Berhasil di Verifikasi")
            
            return redirect("/login")
            
        else:
            return redirect("/error")
        
    except Exception as e:
        print(e)
        return redirect("/")
        
def change_password(request, token):
    try:
        
        profile_obj = Profile.objects.filter(auth_token = token).first()
        
        context = {
            'profile_obj': profile_obj
        }
        
        if request.method == "GET":
        
            return render(request, "autentikasi/change_password.html", context)
        
        if request.method == "POST":
            password_new = request.POST.get("password_new")
            confirm = request.POST.get("confirm")
            user_id = request.POST.get("user_id")
            
            if password_new == "":
                messages.success(request, "Password Tidak Boleh Kosong")
                return redirect("/change_password/{token}")
            if  confirm == "":
                messages.success(request, "Konfirmasi Password Tidak Boleh Kosong")
                return redirect("/change_password/{token}")
            if password_new != confirm:
                messages.success(request, "Konfirmasi Password Tidak Sesuai")
                return redirect("/change_password/{token}", )
            else:
                user_obj = User.objects.filter(username = user_id).first()
                user_obj.set_password(password_new)
                user_obj.save()
                
                return redirect("/login")
                    
            
    except Exception as e:
        print(e)
        return redirect("/")
        

def send_mail_registration(email, token, fname):
	subject = "Hallo, Member"
	message = f"Selamat Datang {fname} di Aplikasi Pendeteksian Suara Pada Sistem Automatic Exam."f" Silahkan klik link berikut ini http://172.20.10.2:8000/verify/{token}"
	email_from = settings.EMAIL_HOST_USER
	recipient_list = [email]
	send_mail(subject, message, email_from, recipient_list)
	
def send_password(email, token, fname):
	subject = "Hallo, Member"
	message = f"Selamat Datang {fname} di Aplikasi Pendeteksian Suara Pada Sistem Automatic Exam."f" Silahkan klik link berikut ini http://172.20.10.2:8000/change_password/{token}"
	email_from = settings.EMAIL_HOST_USER
	recipient_list = [email]
	send_mail(subject, message, email_from, recipient_list)