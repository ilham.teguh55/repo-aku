from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path("", dashboard, name="dashboard"),
    path("register/", register, name="register"),
    path("login/", login_attempt, name="login"),
    path("verify/<auth_token>", verify, name="verify"),
    path("forget_password", forget_password, name="forget_password"),
    path("change_password/<token>", change_password, name="change_password"),
    path("home/", home, name="home"),
]