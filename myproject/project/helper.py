from django.core.mail import send_mail
from django.conf import settings

def send_forget_password_mail(email, token):
    token
    subject = 'Link Lupa Password'
    message = f'Hi, Silahkan klik link berikut ini http://172.20.10.2:8000/change_password/{token}/'
    email_from = settings.EMAIL_HOST_USER
    recipient_list = [email]
    send_email(subject, message, email_from, recipient_list)
    return True